import java.util.ArrayList;

public class Cart {
	ArrayList<Item> cartItems = new ArrayList<Item>();

	public Cart() {


	}
	// Add item 
	public boolean addItem(int code) {
		if (Menu.itemCodeIsValid(code)) {
			cartItems.add(Menu.getItemByCode(code));
			return true;
		}
		else {
			return false;
		}
	}
	// remove item
	public boolean removeItem(int code) {
		for(Item it: cartItems) {
			if(it.code == code) {
				cartItems.remove(it);
				return true;
			}
		}
		return false;
	}

	public int calculateTotalPrice() {
		int totalPrice = 0;
		for(Item it: cartItems) {
			totalPrice += it.price;
		}
		return totalPrice;
	}

	public int calculateTotalQuantity() {
		return cartItems.size();
	}

	public void printCart() {
		String cart = "";
		int totalPrice = 0;
		for(Item it: cartItems) {
			int quantity = countItemQuantity(it.code);
			cart += quantity + " " + it.name + " �" + (quantity*it.price) + "\n";
			totalPrice += quantity*it.price;
		}
		System.out.println(cart);
		System.out.println("------------");
		System.out.println("Total: �"+totalPrice);
	}

	public int countItemQuantity(int code) {
		int quantity = 0;
		for(Item it: cartItems) {
			if(it.code == code) {
				quantity++;
			}
		}
		return quantity;
	}

}
