import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;


public class Main {
	
	/*
	 * 01) Welcome customer to restaurant
	 * 02) Create a table that indicates the amount of seats we need
	 * 03) go through the table checking if there is any seats that haven't been taken
	 * 04) create a loop that will allocate a seat if the customer wants to auto assign a table
	 * 05) make sure the customer can't go outside of table choices (none available too)
	 * 06) Once the customer has booked onto a table and the program has assigned next,
	 * 	   ask if they'd like to order food from the menu
	 * 07) show the customer the menu [code, item, price] save quantity for how many they select
	 * 08) add item to cart depending on what 'code' they select
	 * 09) do a loop to check the customer hasn't gone outside of the array if so show them invalid input
	 *     and allow them to re-choose
	 * 10) give the customer an option to loop back to the start of the 'menu' method and choose more items.
	 * 11) Once complete and order has been successful, thank customer for visiting.
	 */

	public static int tableLength = 42; //Default number of tables
	public static ArrayList<Table> tableList = new ArrayList<Table>(); //Initialise table list object
	public static Scanner scanner =  new Scanner(System.in);
	public static void main(String[] args) {
		
		
		Scanner input = new Scanner(System.in);
		System.out.println("Welcome, enter your name below");
		
		String customer = input.nextLine();
		System.out.println("Welcome " + customer + "  hope you enjoy your meal!");
		
		
		
	
		//Create number of tables based on tableLength
		for(int i=1; i<=tableLength;i++) {
			tableList.add(new Table(i)); //Initialize number of tables
		}
		// Checking if any seats are available before running
		if(getNumberAvailableTables() == 0) {
			System.out.println("Sorry, there currently are no available seats, please try again later");			
		}
		//Main loop for choosing if seat will be auto assigned or not
		while(true) {
			System.out.println("Would you like to choose your own seat? (y/n) If not one will be assigned to you.");
			String response = scanner.nextLine();
			if(response.toLowerCase().equals("y")) {
				System.out.println("Welcome to my restaurant, please see below for available seats!");
				printTables();
				while(true) {
					System.out.println("Please choose the seat that you would like by entering the seat number:");
					int chosenTable = Integer.parseInt(scanner.nextLine());

					//Chose table outside of available list
					if(chosenTable < 1 || chosenTable > tableLength) {
						System.out.println("The seat number you've chosen is not available at this restaurant");
						//Restart customer choice loop for seat number
						continue;
					}

					//Verify if table is already taken
					if(findTable(chosenTable).getIsTaken() == true) {
						System.out.println("The seat you have chosen is already taken!");
						//Restart customer choice loop for seat number
						continue;
					}

					//Passed all checks, time to reserve seat
					findTable(chosenTable).setTableTaken();
					System.out.println("Your table " + chosenTable + " has been succesfully reserved!");

					// used for testing that setTableTaken is working
					// printTables();

					//Break out of customer choice loop for seat number
					break;
				}
				//Break out of customer choice loop (y/n)
				break;
			}else if (response.toLowerCase().equals("n")) {
				//Auto assign next available seat
				reserveNextAvailable();
				//Break out of customer choice loop (y/n)
				break;
			}else {
				System.out.println("You've entered an invalid response.");
				//Restart while(true) loop to give customer another chance to enter response
				continue;
			}
		}

		String orderAnythingElse ="";
		while(true) {
			if(orderAnythingElse.toLowerCase().equals("n")) {
				break;
			}
			Cart cart = new Cart();
			Menu.fillMenu();
			System.out.println("Would you like to order anything?(y/n)");
			String response = scanner.nextLine();
			if(response.toLowerCase().equals("n")) {
				System.out.println("Thank you for visiting our restaurant!");
				break;
			}else {
				while(true) {
					System.out.println("Here is our menu:");
					Menu.printMenu();
					System.out.println("Please enter the code of the item you would like to add to your cart");
					int itemCode = scanner.nextInt();
					if(Menu.itemCodeIsValid(itemCode)) {
						cart.addItem(itemCode);
						System.out.println("The item has been added");
						cart.printCart();
					}else {
						System.out.println("The item code you chose is invalid");
						continue;
					}
					System.out.println("Would you like to order anything else?(y/n)");
					Scanner stringScanner = new Scanner(System.in);
					orderAnythingElse = stringScanner.next();
					if(orderAnythingElse.toLowerCase().equals("n")) {
						break;
						//Go to checkout
					}
					else {
						//Restarting menu loop
						continue;
					}
				}

				

				//Beginning of checkout
				//Print cart
				System.out.println("CHECKOUT");
				cart.printCart();
				while(true) {
				Scanner input1 = new Scanner(System.in);
				System.out.println("Please enter the amount you'd like to pay with in �");
				double payment = input1.nextDouble();
				if(payment >= cart.calculateTotalPrice()) {
					System.out.println("Your payment is �" + payment + ", " + "here is your change: " + "�" + (payment-cart.calculateTotalPrice()));	 
					break;
				}
				else {
				System.out.println("amount isn't high enough total = �" + cart.calculateTotalPrice());	
				break;
				}
				
						           
				
				}
				System.out.println("- - - - - - - - - - Final summary - - - - - - - - - -");
				System.out.println(customer + " your table is fully booked and your order is now complete \n");
				printTables();
				System.out.println("\n");
				cart.printCart();
				 
				}
			
			}
		
		
		
	}
	
	

	
	// print table 
	public static void printTables() {
		DecimalFormat df = new DecimalFormat("00");
		for(Table t: tableList) {
			if(t.getIsTaken() == true) {
				System.out.print("[XX]");
			}else {
				System.out.print("["+df.format(t.getTableNumber())+"]"); //Format print to use 2 decimal values
			}
			if(t.getTableNumber() % 7 == 0) { //Enter new line at every 7 tables
				System.out.print("\n");
			}
		}
	}

	 
	public static void reserveTable(int tableNumber) {
		findTable(tableNumber).setTableTaken();
	}

	public static void reserveNextAvailable() {
		for(Table table : tableList) {
			if(table.getIsTaken() == false) {
				table.setTableTaken();
				return;
			}
		}
	}

	public static int getNumberAvailableTables() {
		int availableTables = 0;
		for(Table table : tableList) {
			if(table.getIsTaken() == false) {
				availableTables++;
			}
		}
		return availableTables;
	}

	public static Table findTable(int tableNumber) {
		for(Table table : tableList) {
			if(tableNumber == table.getTableNumber()) {
				return table;
			}
		}
		return null;
	}

}

