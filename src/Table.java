
public class Table {
	private int tableNumber;
	private boolean isTaken;
	
	public Table() {
		
	}
	
	public Table(int tableNumber) {
		this.tableNumber = tableNumber;
		 isTaken = false;
	}
	
	public int getTableNumber() {
		return tableNumber;
	}
	
	public void setTableTaken() {
		this.isTaken = true;
	}
	
	public boolean getIsTaken() {
		if(isTaken == true) {
			return true;
		}else {
			return false;
		}
	}

}
