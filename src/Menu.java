import java.util.ArrayList;

public class Menu {
	static ArrayList<Item> menuItems = new ArrayList<Item>();
	public Menu() {
		fillMenu();
	}

	public static void fillMenu() {
		menuItems.add(new Item(1,"Pizza",10));
		menuItems.add(new Item(2," Steak",7));
		menuItems.add(new Item(3," Sandwhich",5));
		menuItems.add(new Item(4," Water",1));
		menuItems.add(new Item(5," Soft drink",2));
		menuItems.add(new Item(6," Tea",2));
		menuItems.add(new Item(7," Coffee",2));
		menuItems.add(new Item(8," Ice cream",2));
		menuItems.add(new Item(9," Chocolate",2));
	}
	/*
	public static int getPrice(int cd) {
		for(Item it: menuItems) {
			if(it.code == cd) {
				return it.price;
			}
		}
		return 0;
	}
	*/
	public static Item getItemByCode(int cd) {
		for(Item it: menuItems) {
			if(it.code == cd) {
				return it;
			}
		}
		return null;
	}
	
	public static boolean itemCodeIsValid(int cd) {
		for(Item it: menuItems) {
			if(it.code == cd) {
				return true;
			}
		}
		return false;
	}
	// menu print out
	public static void printMenu() {
		String menu = "";
		menu += "~ ~ > MENU < ~ ~\nCode|Price|Name\n";
		for(Item it: menuItems) {
			menu += it.code + "    " + "�"+it.price + "   " + it.name + "\n";
		}
		System.out.println(menu);
	}
}
